"""
Flask application for managing a list of books.
"""

from flask import Flask, jsonify, request, render_template, redirect

app = Flask(__name__)

books = [
    {
        "id": 1,
        "title": "To Kill a Mockingbird",
        "author": "Harper Lee",
        "pages": 281,
        "genre": "Fiction",
        "summary": (
            "Set in the 1930s Deep South, a young girl named Scout Finch lives with her "
            "brother, Jem, and their father, Atticus. Atticus is a lawyer who is defending "
            "a black man accused of raping a white woman. The story explores themes of "
            "racial injustice and moral growth."
        )
    },
    {
        "id": 2,
        "title": "1984",
        "author": "George Orwell",
        "pages": 328,
        "genre": "Dystopian",
        "summary": (
            "A dystopian novel set in a totalitarian society ruled by Big Brother. The "
            "protagonist, Winston Smith, works for the Ministry of Truth, where he alters "
            "historical records. The novel explores themes of surveillance, propaganda, "
            "and individuality."
        )
    },
    {
        "id": 3,
        "title": "The Great Gatsby",
        "author": "F. Scott Fitzgerald",
        "pages": 180,
        "genre": "Classic",
        "summary": (
            "The story of the mysterious millionaire Jay Gatsby and his obsession with "
            "the beautiful Daisy Buchanan. Set in the Roaring Twenties, it explores themes "
            "of decadence, idealism, and the American Dream."
        )
    },
    {
        "id": 4,
        "title": "Pride and Prejudice",
        "author": "Jane Austen",
        "pages": 279,
        "genre": "Romance",
        "summary": (
            "A romantic novel that follows the life of Elizabeth Bennet as she deals with "
            "issues of manners, upbringing, and marriage. The story highlights the romantic "
            "tension between Elizabeth and Mr. Darcy, and critiques the British landed "
            "gentry at the end of the 18th century."
        )
    },
    {
        "id": 5,
        "title": "Moby Dick",
        "author": "Herman Melville",
        "pages": 635,
        "genre": "Adventure",
        "summary": (
            "The narrative of Captain Ahab's obsessive quest to avenge the whale that "
            "'reaped' his leg. The novel explores themes of fate, the nature of evil, "
            "and the unknown."
        )
    }
]

@app.get('/')
def index():
    """
    Render the index page with the list of books.
    """
    return render_template('index.html', data=books)

@app.get('/books')
def get_books():
    """
    Return the list of all books in JSON format.
    """
    return jsonify(books)

@app.get('/book/<int:book_id>')
def get_book(book_id):
    """
    Return a single book by its ID in JSON format.
    Args:
        book_id (int): The ID of the book to retrieve.
    Returns:
        JSON response with the book data or 404 if not found.
    """
    for book in books:
        if book["id"] == book_id:
            return jsonify(book)
    return f'Book with id {book_id} not found', 404

@app.route('/add_book', methods=['GET', 'POST'])
def add_book():
    """
    Add a new book to the list.
    """
    if request.method == 'POST':
        new_book = {
            "id": int(request.form['id']),
            "title": request.form['title'],
            "author": request.form['author'],
            "pages": int(request.form['pages']),
            "genre": request.form['genre'],
            "summary": request.form['summary']
        }
        books.append(new_book)
        return redirect('/')
    return render_template('add_book.html')

@app.route('/update_book/<int:book_id>', methods=['GET', 'POST'])
def update_book(book_id):
    """
    Update an existing book in the list.
    Args:
        book_id (int): The ID of the book to update.
    """
    for book in books:
        if book["id"] == book_id:
            if request.method == "POST":
                book["id"] = int(request.form['id'])
                book["title"] = request.form['title']
                book["author"] = request.form['author']
                book["pages"] = int(request.form['pages'])
                book["genre"] = request.form['genre']
                book["summary"] = request.form['summary']
                return redirect('/')
            return render_template('update.html', book=book)
    return f'Book with id {book_id} not found', 404

@app.route('/delete_book/<int:book_id>', methods=['GET', 'POST'])
def delete_book(book_id):
    """
    Delete an existing book from the list.
    Args:
        book_id (int): The ID of the book to delete.
    """
    for book in books[:]:
        if book["id"] == book_id:
            if request.method == "POST":
                books.remove(book)
                return redirect('/')
            return render_template('delete.html', book=book)
    return f'Book with id {book_id} not found', 404

if __name__ == '__main__':
    app.run(debug=True)
