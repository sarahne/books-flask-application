"""
Unit tests for the Flask book application.
"""

import pytest
from app import app

@pytest.fixture(name="client")
def client_fixture():
    """
    Fixture to set up the test client for the Flask application.
    """
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_index(client):
    """
    Test the GET / endpoint to ensure it returns a 200 status code.

    Args:
        client: The test client fixture.
    """
    rv = client.get('/')
    assert rv.status_code == 200

def test_get_books(client):
    """
    Test the GET /books endpoint to retrieve the list of books.

    Args:
        client: The test client fixture.
    """
    rv = client.get('/books')
    assert rv.status_code == 200
    assert len(rv.get_json()) > 0

def test_get_book(client):
    """
    Test the GET /book/<int:book_id> endpoint to retrieve a single book.

    Args:
        client: The test client fixture.
    """
    rv = client.get('/book/1')
    assert rv.status_code == 200
    book = rv.get_json()
    assert book['id'] == 1
    assert book['title'] == "To Kill a Mockingbird"

def test_add_book(client):
    """
    Test the POST /add_book endpoint to add a new book.

    Args:
        client: The test client fixture.
    """
    book_data = {
        'id': 6,
        'title': 'The Catcher in the Rye',
        'author': 'J.D. Salinger',
        'pages': 214,
        'genre': 'Fiction',
        'summary': 'A story about teenage rebellion and angst.'
    }
    rv = client.post('/add_book', data=book_data)
    assert rv.status_code == 302  # Redirect to home
    rv = client.get('/books')
    books = rv.get_json()
    assert any(book['title'] == 'The Catcher in the Rye' for book in books)

def test_update_book(client):
    """
    Test the POST /update_book/<int:book_id> endpoint to update an existing book.

    Args:
        client: The test client fixture.
    """
    update_data = {
        'id': 1,
        'title': 'To Kill a Mockingbird (Updated)',
        'author': 'Harper Lee',
        'pages': 290,
        'genre': 'Fiction',
        'summary': 'Updated summary of To Kill a Mockingbird.'
    }
    rv = client.post('/update_book/1', data=update_data)
    assert rv.status_code == 302  # Redirect to home
    rv = client.get('/book/1')
    book = rv.get_json()
    assert book['title'] == 'To Kill a Mockingbird (Updated)'

def test_delete_book(client):
    """
    Test the POST /delete_book/<int:book_id> endpoint to delete a book.

    Args:
        client: The test client fixture.
    """
    rv = client.post('/delete_book/1')
    assert rv.status_code == 302  # Redirect to home
    rv = client.get('/books')
    books = rv.get_json()
    assert not any(book['id'] == 1 for book in books)
